# yafetch

yet another fetch clone written in C++

## Dependencies

- clang
- make

## Configuration

specify which logo to use in `config.h`

## Build instructions

`$ make`		to compile the application <br>
`# make install`	to install the compiled binary <br>
`# make uninstall`	to uninstall the application <br>

## Credits

- icons from [ufetch](https://gitlab.com/jschx/ufetch)
